# Swarm/Docker Demo

Customize your message by modifying the `displayName` in `swarm-demo.yml`.  To then deploy this demo app:
```
#!bash

mvn clean package;
docker build -t swarm-demo .;
docker run -p 8080:8080 swarm-demo;
```

To access the resulting hello world service:
```
#!http

GET http://localhost:8080/hello HTTP/1.1
```
