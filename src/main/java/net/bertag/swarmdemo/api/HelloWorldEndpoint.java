package net.bertag.swarmdemo.api;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import net.bertag.swarmdemo.Configuration;

@Path("/hello")
public class HelloWorldEndpoint {
	
	@Inject private Configuration config;

	@GET
	@Produces("text/plain")
	public Response doGet() {
		return Response.ok("Hello from WildFly Swarm! " + config.getDisplayName()).build();
	}

}
